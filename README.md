# SolHTTP

A simple shard for a web backend, combining [Kemal](https://github.com/kemalcr/kemal) for serving, [Granite](https://github.com/amberframework/granite) for database, plus some additional sugar on top of both.

Includes an authentication cycle (WIP) and a function for session-based messages, also referred to as "flashes".

Written to migrate from [Flask](https://flask.palletsprojects.com/en/1.1.x/), and thus emulates some of its concepts.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     solhttp:
       gitlab: rymiel/solhttp
   ```

2. Run `shards install`

## Usage

```crystal
require "solhttp"

get "/" do end # Setup routes...

SolHTTP.run # Runs granite and kemal and everything else
```

## Development

While I've written this for personal use to rewrite old Flask server code it's here with an open source and license for use, however, I'm not an expert in secure authentication and the sorts, so please open an issue for obvious vulnerabilities!

## Contributing

1. Fork it (<https://gitlab.com/rymiel/solhttp/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [rymiel](https://gitlab.com/rymiel) - creator and maintainer
