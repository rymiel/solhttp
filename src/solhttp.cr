require "./solhttp/config"
require "./solhttp/helper"
require "./solhttp/ext"
require "./solhttp/middleware"

module SolHTTP
  VERSION = {{ `shards version #{__DIR__}`.chomp.stringify }}
end
