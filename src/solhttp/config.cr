module SolHTTP
  Log = ::Log.for("sol")
  class Config
    INSTANCE = self.new

    property env, secret

    def initialize
      @env = "production"
      @secret = "1234567890"
    end
  end

  def self.config
    Config::INSTANCE
  end
end