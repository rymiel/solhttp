require "kemal-session"

class HTTP::Server::Context
  class FlashContainer < Array(Tuple(String, String))
    include Kemal::Session::StorableObject
  end

  private def flash_container
    (session.object?("flash") || FlashContainer.new).as FlashContainer
  end

  def flash(message : String, category : String = "message")
    cur = flash_container
    cur << {message, category}
    session.object("flash", cur)
  end

  def has_messages
    !session.object?("flash").nil?
  end

  def messages
    flashes = flash_container
    session.object("flash", FlashContainer.new) # Clear flashes when read
    flashes
  end

  def messages(&)
    messages.each do |i| yield *i end
  end
end