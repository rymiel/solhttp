require "kemal"
require "kemal-session"
require "./config"
require "./ext"

macro templ(*filenames)
  {% for f, i in filenames + ["base"] %}
    {% if i == 0 %}
      __content_filename__ = "src/views/#{{{f}}}.html.ecr"
    {% end %}
    content_io = IO::Memory.new
    ECR.embed "src/views/#{{{f}}}.html.ecr", content_io
    content = content_io.to_s
  {% end %}
end

Dir.mkdir("./sess") unless Dir.exists? "./sess"
Kemal::Session.config.engine = Kemal::Session::FileEngine.new({:sessions_dir => "./sess"})
Kemal::Session.config.secret = SolHTTP.config.secret
Kemal.config.env = SolHTTP.config.env
Kemal.config.powered_by_header = false
Kemal.config.logger = SolHTTP::LogHandler.new

error 404 {}
error 500 {}

module SolHTTP
  extend self

  def run
    Kemal.run
  end

  class LogHandler < Kemal::BaseLogHandler
    def initialize(@log = ::Log.for("kemal"))
    end

    def call(ctx : HTTP::Server::Context)
      elapsed_time = Time.measure { call_next(ctx) }
      elapsed_text = elapsed_text(elapsed_time)
      @log.info { "#{ctx.response.status_code}\t#{ctx.request.method}\t#{ctx.request.resource}\t#{elapsed_text}" }
      ctx
    end

    def write(message : String)
      @log.warn { message.strip }
    end

    private def elapsed_text(elapsed)
      "#{(elapsed.total_milliseconds * 1000).round(2)}µs"
    end
  end
end
