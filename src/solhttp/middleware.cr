require "http/server"
require "kemal"

module SolHTTP::Middleware
  abstract class AbstractHandler
    getter common_prefix : String

    def initialize(@common_prefix)
      paths
    end

    private def relative_path(suffix : String)
      if suffix.empty?
        common_prefix
      elsif suffix[0] == "/"
        suffix
      else
        common_prefix + "/" + suffix
      end
    end

    def paths
    end

    {% for method in HTTP_METHODS %}
      def hook_{{method.id}}(ctx : HTTP::Server::Context)
        true
      end
    {% end %}

    def hook_all(ctx : HTTP::Server::Context) : Bool
      true
    end

    {% for method in HTTP_METHODS %}
      def {{method.id}}(path : String, &block : HTTP::Server::Context -> _)
        ::{{method.id}} relative_path(path) do |ctx|
          next unless self.hook_all ctx
          next unless self.hook_{{method.id}} ctx
          block.call ctx
        end
      end
    {% end %}
  end
end
